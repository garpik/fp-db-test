<?php

namespace FpDbTest\Database;

use FpDbTest\FieldTypes\Interfaces\SkipInterface;
use FpDbTest\Database\Interfaces\QueryBuilderInterface;
use Generator;
use Exception;
use mysqli;

class QueryBuilder implements QueryBuilderInterface
{


    /**
     * @var TypesConverter
     */
    protected TypesConverter $typesConverter;

    /**
     * @param mysqli $mysqli
     */
    public function __construct(protected mysqli $mysqli)
    {
        $this->typesConverter = new TypesConverter($this->mysqli);
    }

    /**
     * @param Generator $blocks
     * @param array $args
     * @return string
     */
    public function handleQueryBlocks(Generator $blocks, array $args = []): string
    {
        $resultQuery = "";
        foreach ($blocks as $sqlQuery) {
            $resultQuery .= $this->getLogicBlockData($sqlQuery, $args);
        }
        return $resultQuery;
    }

    /**
     * @param string $sqlQuery
     * @return Generator
     * @throws Exception
     */
    public function splitToLogicBlocks(string $sqlQuery = ''): Generator
    {
        $curPos = 0;
        while (($matchPos = strpos($sqlQuery, '{', $curPos)) !== false) {
            yield substr($sqlQuery, $curPos, $matchPos - $curPos);
            $closeBlockPosition = strpos($sqlQuery, '}', ++$matchPos);
            if ($closeBlockPosition === false) {
                throw new Exception("Brackets are not closed");
            }
            if (
                ($nextBlockOpening = strpos($sqlQuery, '{', $matchPos)) !== false &&
                $closeBlockPosition > $nextBlockOpening
            ) {
                throw new Exception("The block is invalid");
            }
            yield substr($sqlQuery, $matchPos, $closeBlockPosition - $matchPos);
            $curPos = ++$closeBlockPosition;
        }
        if (strpos($sqlQuery, '}', $curPos) !== false) {
            throw new Exception("The block is invalid");
        }
        if ($curPos !== strlen($sqlQuery)) {
            yield substr($sqlQuery, $curPos, strlen($sqlQuery));
        }
    }

    /**
     * @param string $sqlQuery
     * @param array $data
     * @return string
     */
    private function getLogicBlockData(string $sqlQuery = '', array &$data = []): string
    {
        $matchPosition = 0;
        while (($matchPosition = strpos($sqlQuery, "?", $matchPosition)) !== false) {
            $stringValue = substr($sqlQuery, $matchPosition, 2);
            $values = array_shift($data);
            if ($values instanceof SkipInterface) {
                return "";
            }
            $type = $this->typesConverter->getType($stringValue);
            $finalValue = $this->typesConverter->convertElementToType($values, $type);
            $typeLength = ($type == $this->typesConverter->getDefaultType() ? 1 : 2);
            $sqlQuery = substr_replace($sqlQuery, $finalValue, $matchPosition, $typeLength);
            $matchPosition++;
        }
        return $sqlQuery;
    }

}