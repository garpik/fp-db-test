<?php

namespace FpDbTest\Database;

use FpDbTest\Database\Interfaces\TypesConverterInterface;

class TypesConverter implements TypesConverterInterface
{

    private const string INTEGER_TYPE = "integer";
    private const string FLOAT_TYPE = "float";
    private const string ARRAY_TYPE = "array";
    private const string IDENTIFIER_TYPE = "identifier";
    private const string DEFAULT_TYPE = "default";
    private const array TYPES_SPECIFICATION = [
        "?d" => self::INTEGER_TYPE,
        "?f" => self::FLOAT_TYPE,
        "?a" => self::ARRAY_TYPE,
        "?#" => self::IDENTIFIER_TYPE,
        "?" => self::DEFAULT_TYPE
    ];


    public function __construct(protected \mysqli $mysqli)
    {
    }

    /**
     * @return string
     */
    public function getDefaultType(): string
    {
        return self::DEFAULT_TYPE;
    }

    /**
     * @param string $typeString
     * @return string
     */
    public function getType(string $typeString = ''): string
    {
        return self::TYPES_SPECIFICATION[$typeString] ?? self::DEFAULT_TYPE;
    }

    /**
     * @param mixed|null $value
     * @param string $type
     * @return string|int|float
     */
    public function convertElementToType(mixed $value = null, string $type = ""): string|int|float
    {
        return match ($type) {
            self::INTEGER_TYPE => is_null($value) ? "NULL" : (integer)$value,
            self::FLOAT_TYPE => is_null($value) ? "NULL" : (double)$value,
            self::IDENTIFIER_TYPE => is_array($value) ?
                $this->identifierFromArray($value) :
                "`$value`",
            self::ARRAY_TYPE => array_is_list($value) ?
                implode(', ', $value) :
                $this->convertArrayToString($value),
            default => $this->autoTypeConverter($value)
        };
    }

    /**
     * @param array $data
     * @return string
     */
    private function convertArrayToString(array $data = []): string
    {
        $resultArray = "";
        foreach ($data as $key => &$value) {
            if ($value === null) {
                $value = "NULL";
            } else {
                $value = $this->mysqli->real_escape_string($value);
                $value = "'$value'";
            }
            $resultArray .= "`$key` = $value, ";
        }
        return rtrim($resultArray, ', ');
    }

    /**
     * @param mixed $value
     * @return string|int|float
     */
    private function autoTypeConverter(mixed $value): string|int|float
    {
        return match (true) {
            is_null($value) => "NULL",
            is_numeric($value) => $value,
            is_bool($value) => $value ? "1" : "0",
            is_array($value) => array_is_list($value) ? implode(', ', $value) : $this->convertArrayToString($value),
            is_string($value) => "'" . $this->mysqli->real_escape_string($value) . "'",
            default => ""
        };
    }

    /**
     * @param array $identifiers
     * @return string
     */
    private function identifierFromArray(array $identifiers = []): string
    {
        $identifiers = array_map(
            function ($value) {
                return "`$value`";
            },
            $identifiers
        );
        return implode(', ', $identifiers);
    }
}