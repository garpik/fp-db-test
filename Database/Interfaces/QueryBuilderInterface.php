<?php

namespace FpDbTest\Database\Interfaces;

use Generator;

interface QueryBuilderInterface
{

    /**
     * @param string $sqlQuery
     * @return Generator
     */
    public function splitToLogicBlocks(string $sqlQuery = ''): Generator;

    /**
     * @param Generator $blocks
     * @param array $args
     * @return string
     */
    public function handleQueryBlocks(Generator $blocks, array $args = []): string;
}