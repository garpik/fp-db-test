<?php

namespace FpDbTest\Database\Interfaces;

interface TypesConverterInterface
{

    /**
     * @return string
     */
    public function getDefaultType(): string;

    /**
     * @param string $typeString
     * @return string
     */
    public function getType(string $typeString = ''): string;

    /**
     * @param mixed|null $value
     * @param string $type
     * @return string|int|float
     */
    public function convertElementToType(mixed $value = null, string $type = ""): string|int|float;
}