<?php

namespace FpDbTest;

use Exception;
use FpDbTest\Database\QueryBuilder;
use FpDbTest\FieldTypes\Skip;
use mysqli;

class Database implements DatabaseInterface
{

    /**
     * @var Database\QueryBuilder
     */
    private QueryBuilder $queryBuilder;

    /**
     * @param mysqli $mysqli
     */
    public function __construct(protected mysqli $mysqli)
    {
        $this->queryBuilder = new QueryBuilder($this->mysqli);
    }

    /**
     * @param string $query
     * @param array $args
     * @return string
     * @throws Exception
     */
    public function buildQuery(string $query, array $args = []): string
    {
        $logicBlocks = $this->queryBuilder->splitToLogicBlocks($query);
        return $this->queryBuilder->handleQueryBlocks($logicBlocks, $args);
    }

    /**
     * @return Skip
     */
    public function skip(): Skip
    {
        return new Skip();
    }

}
